//
//  ViewController.swift
//  CoreMLSample
//
//  Created by Nguyen Loc on 5/6/18.
//  Copyright © 2018 Nguyen Loc. All rights reserved.
//

import UIKit
import AVKit
import CoreML
import Vision

class ViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    @IBOutlet weak var lblOutput: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let captureSession = AVCaptureSession()
        captureSession.sessionPreset = .photo
        guard let captureDevice = AVCaptureDevice.default(for: .video) else {
            return
        }
        guard let captureInput = try? AVCaptureDeviceInput(device: captureDevice) else {
            return
        }
        captureSession.addInput(captureInput)
        let outputLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        self.view.layer.addSublayer(outputLayer)
        outputLayer.frame = self.view.frame
        captureSession.startRunning()
        
        let captureOutput = AVCaptureVideoDataOutput()
        captureOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "videoOutput"))
        captureSession.addOutput(captureOutput)
        
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let mlModel = try? VNCoreMLModel(for: Resnet50().model) else {return}
        let request: VNRequest = VNCoreMLRequest(model: mlModel) { (request, err) in
            print(request.results)
            DispatchQueue.main.async {
                let result = request.results?.first as! VNClassificationObservation
                self.lblOutput.text = result.identifier + " conf: " + String(result.confidence)
            }
        }
        guard let pixelBuffer:CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        try? VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:]).perform([request])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

